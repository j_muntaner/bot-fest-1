exports.bot = function(hp, history, ties, alive, start) {
	if (alive == 2) {
		return hp - 1 + ties
	}

	let enemyRemainingLife = 100 - history.reduce((a, b) => a + b, 0)

	return 0.6 * hp + 1 + ties
}