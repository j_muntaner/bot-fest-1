exports.bot = function(hp, history, ties, alive, start) {
	if (alive == 2) {
		return hp - 1 + ties
	}

	let enemyRemainingLife = 100 - history.reduce((a, b) => a + b, 0)

	return Math.min((random(0.4,0.5)*hp + random(0.1,0.25)*enemyRemainingLife+random(1,3)), 0.70*hp, 0.76*enemyRemainingLife)
}

function random(min, max) {
	return Math.random() * (max - min) + min
}