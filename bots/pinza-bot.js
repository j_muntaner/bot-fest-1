exports.bot = function(hp, history, ties, alive, start) {
	if (alive == 2) {
		return hp - 1 + ties
	}
	
    let enemyHp = 100 - history.reduce((a, b) => a + b, 0)
	return Math.min(Math.round(Math.sqrt(1/2*Math.pow(enemyHp, 2)+Math.pow(hp, 2))) / 2, hp*0.75) + ties
}